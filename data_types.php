<?php
    //boolean starts here

    $decision=true;
    if($decision){
        echo "The decision is True! <br>";
    }
    $decision=false;

    if($decision){
    echo "The decision is False! <br>";
    }
    //boolean ends here

    //integer & float start here
    $value1=100; //integer

    $value2=55.35; //float

    //integer & float end here

    //string example starts here
    $myString1='abcd123#$% $value1'; //single quoted

    $myString2="abcd123#$% $value1"; //double quoted

    echo $myString1 . "<br>";

    echo $myString2. "<br>";
    //string ends here

    //Heredoc_string start here
    $heredocString=<<<BITM
    heredoc line1 $value1 <br>
    heredoc line2 $value1 <br>
    heredoc line3 $value1 <br>
    heredoc line4 $value1 <br>
BITM;
    echo $heredocString;

    //Heredoc_string ends here

     //Nowdoc_string start here
    $nowdocString=<<<'BITM'
     nowdoc line1 $value1 <br>
     nowdoc line2 $value1 <br>
     nowdoc line3 $value1 <br>
     nowdoc line4 $value1 <br>
BITM;
    echo $nowdocString;
      //Nowdoc_string ends here
?>

